package ru.mtumanov.tm.exception.field;

public final class IndexIncorectException extends AbstractFieldException {

    public IndexIncorectException() {
        super("ERROR! Index is empty!");
    }

}
