package ru.mtumanov.tm.exception.user;

public class PermissionException extends AbstractUserException {

    public PermissionException() {
        super("ERROR! Permission is incorrect!");
    }

}
