package ru.mtumanov.tm.dto.response.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.dto.response.AbstractRs;
import ru.mtumanov.tm.model.Task;

import java.util.List;

@Getter
@NoArgsConstructor
public class TaskListByProjectIdRs extends AbstractRs {

    @Nullable
    private List<Task> tasks;

    public TaskListByProjectIdRs(@Nullable final List<Task> tasks) {
        this.tasks = tasks;
    }

}