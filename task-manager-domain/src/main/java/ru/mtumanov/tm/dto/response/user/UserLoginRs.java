package ru.mtumanov.tm.dto.response.user;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.mtumanov.tm.dto.response.AbstractResultRs;

@Getter
@Setter
@NoArgsConstructor
public final class UserLoginRs extends AbstractResultRs {

    @NotNull
    private String token;

    public UserLoginRs(@NotNull String token) {
        this.token = token;
    }

    public UserLoginRs(@NotNull Throwable throwable) {
        super(throwable);
    }

}