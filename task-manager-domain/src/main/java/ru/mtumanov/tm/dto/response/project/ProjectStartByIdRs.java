package ru.mtumanov.tm.dto.response.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.model.Project;

@NoArgsConstructor
public class ProjectStartByIdRs extends AbstractProjectRs {

    public ProjectStartByIdRs(@Nullable final Project project) {
        super(project);
    }

    public ProjectStartByIdRs(@Nullable final Throwable err) {
        super(err);
    }

}
