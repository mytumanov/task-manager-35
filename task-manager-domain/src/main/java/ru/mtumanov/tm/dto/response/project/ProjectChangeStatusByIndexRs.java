package ru.mtumanov.tm.dto.response.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.model.Project;

@NoArgsConstructor
public final class ProjectChangeStatusByIndexRs extends AbstractProjectRs {

    public ProjectChangeStatusByIndexRs(@Nullable final Project project) {
        super(project);
    }

    public ProjectChangeStatusByIndexRs(@NotNull final Throwable err) {
        super(err);
    }

}
