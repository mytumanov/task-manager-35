package ru.mtumanov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import ru.mtumanov.tm.api.repository.IProjectRepository;
import ru.mtumanov.tm.api.repository.ITaskRepository;
import ru.mtumanov.tm.api.service.IProjectTaskService;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.exception.entity.ProjectNotFoundException;
import ru.mtumanov.tm.exception.field.IdEmptyException;
import ru.mtumanov.tm.model.Project;
import ru.mtumanov.tm.model.Task;
import ru.mtumanov.tm.repository.ProjectRepository;
import ru.mtumanov.tm.repository.TaskRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.junit.Assert.*;

public class ProjectTaskServiceTest {

    private static final int NUMBER_OF_ENTRIES = 10;

    @NotNull
    private final String USER_ID = UUID.randomUUID().toString();

    @NotNull
    private static final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private static final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private static final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    @NotNull
    private List<Task> bindedTasks;

    @NotNull
    private List<Task> unbindedTasks;

    @NotNull
    private static final Project project = new Project();

    @Before
    public void initRepository() {
        bindedTasks = new ArrayList<>();
        unbindedTasks = new ArrayList<>();
        project.setName("PROJECT");
        project.setDescription("PROject description");
        project.setUserId(USER_ID);
        projectRepository.add(project);

        for (int i = 0; i < NUMBER_OF_ENTRIES; i++) {
            @NotNull final Task task = new Task();
            task.setName("Task binded name: " + i);
            task.setDescription("Task binded description: " + i);
            task.setUserId(USER_ID);
            task.setProjectId(project.getId());
            taskRepository.add(task);
            bindedTasks.add(task);
        }

        for (int i = 0; i < NUMBER_OF_ENTRIES; i++) {
            @NotNull final Task task = new Task();
            task.setName("Task unbinded name: " + i);
            task.setDescription("Task unbinded description: " + i);
            task.setUserId(USER_ID);
            taskRepository.add(task);
            unbindedTasks.add(task);
        }
    }

    @After
    public void clearRepository() {
        taskRepository.clear();
        projectRepository.clear();
        bindedTasks.clear();
        unbindedTasks.clear();
    }

    @Test
    public void testBindTaskToProject() throws AbstractException {
        for (@NotNull final Task task : unbindedTasks) {
            assertNull(task.getProjectId());
            projectTaskService.bindTaskToProject(USER_ID, project.getId(), task.getId());
            assertEquals(project.getId(), task.getProjectId());
        }
    }

    @Test(expected = IdEmptyException.class)
    public void testPrjEmptyBindTaskToProject() throws AbstractException {
        projectTaskService.bindTaskToProject(USER_ID, "", "123");
    }

    @Test(expected = IdEmptyException.class)
    public void testTskEmptyBindTaskToProject() throws AbstractException {
        projectTaskService.bindTaskToProject(USER_ID, "123", "");
    }

    @Test(expected = ProjectNotFoundException.class)
    public void testBadUseryBindTaskToProject() throws AbstractException {
        projectTaskService.bindTaskToProject("123", project.getId(), "123");
    }

    @Test
    public void testRemoveProjectById() throws AbstractException {
        assertNotEquals(0, taskRepository.findAllByProjectId(USER_ID, project.getId()).size());
        assertTrue(projectRepository.existById(project.getId()));
        projectTaskService.removeProjectById(USER_ID, project.getId());
        assertFalse(projectRepository.existById(project.getId()));
        assertEquals(0, taskRepository.findAllByProjectId(USER_ID, project.getId()).size());
    }

    @Test(expected = IdEmptyException.class)
    public void testProjectEmptyRemoveProjectById() throws AbstractException {
        projectTaskService.removeProjectById(USER_ID, "");
    }

    @Test(expected = ProjectNotFoundException.class)
    public void testProjectBadRemoveProjectById() throws AbstractException {
        projectTaskService.removeProjectById(USER_ID, "123");
    }

    @Test
    public void testUnbindTaskFromProject() throws AbstractException {
        for (@NotNull final Task task : bindedTasks) {
            assertEquals(project.getId(), task.getProjectId());
            projectTaskService.unbindTaskFromProject(USER_ID, project.getId(), task.getId());
            assertNull(task.getProjectId());
        }
    }

    @Test(expected = IdEmptyException.class)
    public void testPrjEmptyUnbindTaskFromProject() throws AbstractException {
        projectTaskService.bindTaskToProject(USER_ID, "", "123");
    }

    @Test(expected = IdEmptyException.class)
    public void testTskEmptyUnbindTaskFromProject() throws AbstractException {
        projectTaskService.bindTaskToProject(USER_ID, "123", "");
    }

    @Test(expected = ProjectNotFoundException.class)
    public void testBadUserUnbindTaskFromProject() throws AbstractException {
        projectTaskService.bindTaskToProject("123", project.getId(), "123");
    }
}
