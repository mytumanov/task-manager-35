package ru.mtumanov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import ru.mtumanov.tm.api.repository.IProjectRepository;
import ru.mtumanov.tm.api.service.IProjectService;
import ru.mtumanov.tm.enumerated.Status;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.exception.field.IdEmptyException;
import ru.mtumanov.tm.exception.field.IndexIncorectException;
import ru.mtumanov.tm.exception.field.NameEmptyException;
import ru.mtumanov.tm.exception.user.UserIdEmptyException;
import ru.mtumanov.tm.model.Project;
import ru.mtumanov.tm.repository.ProjectRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.junit.Assert.*;

public class ProjectServiceTest {

    private static final int NUMBER_OF_ENTRIES = 10;

    @NotNull
    private static final String USER_ID_1 = UUID.randomUUID().toString();

    @NotNull
    private static final String USER_ID_2 = UUID.randomUUID().toString();

    @NotNull
    private static final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private static final IProjectService projectService = new ProjectService(projectRepository);

    @NotNull
    private static final List<Project> projectList = new ArrayList<>();

    @Before
    public void initRepository() {
        for (int i = 0; i < NUMBER_OF_ENTRIES; i++) {
            @NotNull final Project project = new Project();
            project.setName("Project name: " + i);
            project.setDescription("Project description: " + i);
            if (i <= 5)
                project.setUserId(USER_ID_1);
            else
                project.setUserId(USER_ID_2);
            projectRepository.add(project);
            projectList.add(project);
        }
    }

    @After
    public void clearRepository() {
        projectRepository.clear();
        projectList.clear();
    }

    @Test
    public void testAdd() throws AbstractException {
        @NotNull final Project project = new Project();
        project.setName("Test project name");
        project.setDescription("Test project description");
        project.setUserId(USER_ID_1);

        projectList.add(project);
        projectService.add(USER_ID_1, project);
        List<Project> actualProjectList = projectService.findAll();
        assertEquals(projectList, actualProjectList);

    }

    @Test(expected = UserIdEmptyException.class)
    public void testExceptionAdd() throws AbstractException {
        projectService.add("", new Project());
    }

    @Test
    public void testClear() throws AbstractException {
        assertNotEquals(0, projectRepository.findAll(USER_ID_1).size());
        projectService.clear(USER_ID_1);
        assertEquals(0, projectRepository.findAll(USER_ID_1).size());
    }

    @Test(expected = UserIdEmptyException.class)
    public void testExceptionClear() throws AbstractException {
        projectService.clear("");
    }

    @Test
    public void TestfindAll() throws AbstractException {
        @NotNull final List<Project> projectsUser1 = new ArrayList<>();
        @NotNull final List<Project> projectsUser2 = new ArrayList<>();
        for (@NotNull final Project project : projectList) {
            if (project.getUserId().equals(USER_ID_1))
                projectsUser1.add(project);
            if (project.getUserId().equals(USER_ID_2))
                projectsUser2.add(project);
        }
        assertNotEquals(projectsUser1, projectsUser2);

        @NotNull final List<Project> actualProjectsUser1 = projectService.findAll(USER_ID_1);
        @NotNull final List<Project> actualProjectsUser2 = projectService.findAll(USER_ID_2);
        @NotNull final List<Project> actualProjectsAll = projectService.findAll();
        assertEquals(projectsUser1, actualProjectsUser1);
        assertEquals(projectsUser2, actualProjectsUser2);
        assertEquals(projectList, actualProjectsAll);
    }

    @Test(expected = UserIdEmptyException.class)
    public void testExceptionfindAll() throws AbstractException {
        projectService.findAll("");
    }

    @Test
    public void testFindOneById() throws AbstractException {
        for (@NotNull final Project project : projectList) {
            assertEquals(project, projectService.findOneById(project.getUserId(), project.getId()));
        }
    }

    @Test(expected = UserIdEmptyException.class)
    public void testEmptyUserFindOneById() throws AbstractException {
        projectService.findOneById("", UUID.randomUUID().toString());
    }

    @Test(expected = IdEmptyException.class)
    public void testEmptyIdFindOneById() throws AbstractException {
        projectService.findOneById(UUID.randomUUID().toString(), "");
    }

    @Test
    public void testFindOneByIndex() throws AbstractException {
        int i = 0;
        int j = 0;
        for (@NotNull final Project project : projectList) {
            if (project.getUserId().equals(USER_ID_1)) {
                assertEquals(project, projectService.findOneByIndex(USER_ID_1, i));
                i++;
            }
            if (project.getUserId().equals(USER_ID_2)) {
                assertEquals(project, projectService.findOneByIndex(USER_ID_2, j));
                j++;
            }
        }
    }

    @Test(expected = IndexIncorectException.class)
    public void testIndexFindOneByIndex() throws AbstractException {
        projectService.findOneByIndex(USER_ID_1, -9);
    }

    @Test(expected = UserIdEmptyException.class)
    public void testUserEmptyFindOneByIndex() throws AbstractException {
        projectService.findOneByIndex("", 0);
    }

    @Test
    public void testRemove() throws AbstractException {
        for (@NotNull final Project project : projectList) {
            assertTrue(projectRepository.existById(project.getId()));
            projectService.remove(project.getUserId(), project);
            assertFalse(projectRepository.existById(project.getId()));
        }
    }

    @Test(expected = UserIdEmptyException.class)
    public void testExceptionRemove() throws AbstractException {
        projectService.remove("", new Project());
    }

    @Test
    public void testRemoveById() throws AbstractException {
        for (@NotNull final Project project : projectList) {
            assertTrue(projectRepository.existById(project.getId()));
            projectService.removeById(project.getUserId(), project.getId());
            assertFalse(projectRepository.existById(project.getId()));
        }
    }

    @Test(expected = UserIdEmptyException.class)
    public void testEmptyUserRemoveById() throws AbstractException {
        projectService.removeById("", UUID.randomUUID().toString());
    }

    @Test(expected = IdEmptyException.class)
    public void testEmptyIdRemoveById() throws AbstractException {
        projectService.removeById(UUID.randomUUID().toString(), "");
    }

    @Test
    public void testRemoveByIndex() throws AbstractException {
        for (@NotNull final Project project : projectList) {
            if (project.getUserId().equals(USER_ID_1)) {
                assertTrue(projectRepository.existById(project.getId()));
                projectService.removeByIndex(project.getUserId(), 0);
                assertFalse(projectRepository.existById(project.getId()));
            }
            if (project.getUserId().equals(USER_ID_2)) {
                assertTrue(projectRepository.existById(project.getId()));
                projectService.removeByIndex(project.getUserId(), 0);
                assertFalse(projectRepository.existById(project.getId()));
            }
        }
    }

    @Test(expected = IndexIncorectException.class)
    public void testIndexRemoveByIndex() throws AbstractException {
        projectService.removeByIndex(USER_ID_1, -9);
    }

    @Test(expected = UserIdEmptyException.class)
    public void testUserEmptyRemoveByIndex() throws AbstractException {
        projectService.removeByIndex("", 0);
    }

    @Test
    public void testExistById() throws AbstractException {
        for (@NotNull final Project project : projectList) {
            assertTrue(projectService.existById(project.getUserId(), project.getId()));
        }
        assertFalse(projectService.existById(USER_ID_1, UUID.randomUUID().toString()));
    }

    @Test(expected = UserIdEmptyException.class)
    public void testUserEmptyExistById() throws AbstractException {
        projectService.existById("", UUID.randomUUID().toString());
    }

    @Test(expected = IdEmptyException.class)
    public void testEmptyIdExistById() throws AbstractException {
        projectService.existById(UUID.randomUUID().toString(), "");
    }

    @Test
    public void testGetSize() throws AbstractException {
        @NotNull final List<Project> projectsUser1 = new ArrayList<>();
        @NotNull final List<Project> projectsUser2 = new ArrayList<>();
        for (@NotNull final Project project : projectList) {
            if (project.getUserId().equals(USER_ID_1))
                projectsUser1.add(project);
            if (project.getUserId().equals(USER_ID_2))
                projectsUser2.add(project);
        }
        assertEquals(projectsUser1.size(), projectService.getSize(USER_ID_1));
        assertEquals(projectsUser2.size(), projectService.getSize(USER_ID_2));
    }

    @Test(expected = UserIdEmptyException.class)
    public void testExceptionGetSize() throws AbstractException {
        projectService.getSize("");
    }

    @Test
    public void testChangeProjectStatusById() throws AbstractException {
        int i = 0;
        for (@NotNull final Project project : projectList) {
            if (i % 2 == 0) {
                projectService.changeProjectStatusById(project.getUserId(), project.getId(), Status.IN_PROGRESS);
                assertEquals(Status.IN_PROGRESS, project.getStatus());
            }
            if (i % 3 == 0) {
                projectService.changeProjectStatusById(project.getUserId(), project.getId(), Status.COMPLETED);
                assertEquals(Status.COMPLETED, project.getStatus());
            }
            i++;
        }
    }

    @Test(expected = IdEmptyException.class)
    public void testExceptionChangeProjectStatusById() throws AbstractException {
        projectService.changeProjectStatusById(UUID.randomUUID().toString(), "", Status.IN_PROGRESS);
    }

    @Test
    public void testChangeProjectStatusByIndex() throws AbstractException {
        @NotNull final List<Project> projectsUser1 = new ArrayList<>();
        @NotNull final List<Project> projectsUser2 = new ArrayList<>();
        for (@NotNull final Project project : projectList) {
            if (project.getUserId().equals(USER_ID_1))
                projectsUser1.add(project);
            if (project.getUserId().equals(USER_ID_2))
                projectsUser2.add(project);
        }

        for (int i = 0; i < projectsUser1.size(); i++) {
            @NotNull final Project project = projectsUser1.get(i);
            if (i % 2 == 0) {
                projectService.changeProjectStatusByIndex(project.getUserId(), i, Status.IN_PROGRESS);
                assertEquals(Status.IN_PROGRESS, project.getStatus());
            }
            if (i % 3 == 0) {
                projectService.changeProjectStatusByIndex(project.getUserId(), i, Status.COMPLETED);
                assertEquals(Status.COMPLETED, project.getStatus());
            }
        }

        for (int i = 0; i < projectsUser2.size(); i++) {
            @NotNull final Project project = projectsUser2.get(i);
            if (i % 2 == 0) {
                projectService.changeProjectStatusByIndex(project.getUserId(), i, Status.IN_PROGRESS);
                assertEquals(Status.IN_PROGRESS, project.getStatus());
            }
            if (i % 3 == 0) {
                projectService.changeProjectStatusByIndex(project.getUserId(), i, Status.COMPLETED);
                assertEquals(Status.COMPLETED, project.getStatus());
            }
        }
    }

    @Test(expected = IndexIncorectException.class)
    public void testExceptionChangeProjectStatusByIndex() throws AbstractException {
        projectService.changeProjectStatusByIndex(UUID.randomUUID().toString(), -10, Status.COMPLETED);
    }

    @Test
    public void testCreate() throws AbstractException {
        @NotNull final String name = "TEST project";
        @NotNull final String description = "DEscription";
        @NotNull final Project project = projectService.create(USER_ID_1, name, description);
        @NotNull final Project actualProject = projectRepository.findOneById(project.getId());
        assertEquals(name, actualProject.getName());
        assertEquals(description, actualProject.getDescription());
        assertEquals(USER_ID_1, actualProject.getUserId());
        assertEquals(projectRepository.getSize(), projectList.size() + 1);
    }

    @Test
    public void testUpdateById() throws AbstractException {
        for (@NotNull final Project project : projectList) {
            @NotNull final String name = project.getName() + "TEST";
            @NotNull final String description = project.getDescription() + "TEST";
            projectService.updateById(project.getUserId(), project.getId(), name, description);
            assertEquals(project.getName(), name);
            assertEquals(project.getDescription(), description);
        }
    }

    @Test(expected = NameEmptyException.class)
    public void testExceptionUpdateById() throws AbstractException {
        projectService.updateById(UUID.randomUUID().toString(), UUID.randomUUID().toString(), "", "");
    }

    @Test
    public void testUpdateByIndex() throws AbstractException {
        @NotNull final List<Project> projectsUser1 = new ArrayList<>();
        @NotNull final List<Project> projectsUser2 = new ArrayList<>();
        for (@NotNull final Project project : projectList) {
            if (project.getUserId().equals(USER_ID_1))
                projectsUser1.add(project);
            if (project.getUserId().equals(USER_ID_2))
                projectsUser2.add(project);
        }

        for (int i = 0; i < projectsUser1.size(); i++) {
            @NotNull final Project project = projectsUser1.get(i);
            @NotNull final String name = project.getName() + "TEST";
            @NotNull final String description = project.getDescription() + "TEST";
            projectService.updateByIndex(USER_ID_1, i, name, description);
            assertEquals(project.getName(), name);
            assertEquals(project.getDescription(), description);
        }

        for (int i = 0; i < projectsUser2.size(); i++) {
            @NotNull final Project project = projectsUser2.get(i);
            @NotNull final String name = project.getName() + "TEST";
            @NotNull final String description = project.getDescription() + "TEST";
            projectService.updateByIndex(USER_ID_2, i, name, description);
            assertEquals(project.getName(), name);
            assertEquals(project.getDescription(), description);
        }
    }

    @Test(expected = NameEmptyException.class)
    public void testExceptionUpdateByIndex() throws AbstractException {
        projectService.updateByIndex(UUID.randomUUID().toString(), 0, "", "");
    }
}
