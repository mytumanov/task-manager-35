package ru.mtumanov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import ru.mtumanov.tm.api.repository.ITaskRepository;
import ru.mtumanov.tm.api.service.ITaskService;
import ru.mtumanov.tm.enumerated.Status;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.exception.field.IdEmptyException;
import ru.mtumanov.tm.exception.field.IndexIncorectException;
import ru.mtumanov.tm.exception.field.NameEmptyException;
import ru.mtumanov.tm.model.Project;
import ru.mtumanov.tm.model.Task;
import ru.mtumanov.tm.repository.TaskRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class TaskServiceTest {

    private static final int NUMBER_OF_ENTRIES = 10;

    @NotNull
    private static final String USER_ID_1 = UUID.randomUUID().toString();

    @NotNull
    private static final String USER_ID_2 = UUID.randomUUID().toString();

    @NotNull
    private static final Project projectUser1 = new Project();

    @NotNull
    private static final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private static final ITaskService taskService = new TaskService(taskRepository);

    @NotNull
    private List<Task> taskList = new ArrayList<>();

    @BeforeClass
    public static void initData() {
        projectUser1.setName("Test task name");
        projectUser1.setDescription("Test task description");
        projectUser1.setUserId(USER_ID_1);
    }

    @Before
    public void initRepository() {
        for (int i = 0; i < NUMBER_OF_ENTRIES; i++) {
            @NotNull final Task task = new Task();
            task.setName("Task name: " + i);
            task.setDescription("Task description: " + i);
            if (i <= 5) {
                task.setUserId(USER_ID_1);
                task.setProjectId(projectUser1.getId());
            } else
                task.setUserId(USER_ID_2);
            taskRepository.add(task);
            taskList.add(task);
        }
    }

    @After
    public void clearRepository() {
        taskRepository.clear();
        taskList.clear();
    }

    @Test
    public void testChangeTaskStatusById() throws AbstractException {
        int i = 0;
        for (@NotNull final Task task : taskList) {
            if (i % 2 == 0) {
                taskService.changeTaskStatusById(task.getUserId(), task.getId(), Status.IN_PROGRESS);
                assertEquals(Status.IN_PROGRESS, task.getStatus());
            }
            if (i % 3 == 0) {
                taskService.changeTaskStatusById(task.getUserId(), task.getId(), Status.COMPLETED);
                assertEquals(Status.COMPLETED, task.getStatus());
            }
            i++;
        }
    }

    @Test(expected = IdEmptyException.class)
    public void testExceptionChangeTaskStatusById() throws AbstractException {
        taskService.changeTaskStatusById(UUID.randomUUID().toString(), "", Status.IN_PROGRESS);
    }

    @Test
    public void testChangeTaskStatusByIndex() throws AbstractException {
        int i = 0;
        for (@NotNull final Task task : taskList) {
            if (task.getUserId().equals(USER_ID_1)) {
                assertNotEquals(Status.IN_PROGRESS, task.getStatus());
                taskService.changeTaskStatusByIndex(USER_ID_1, i, Status.IN_PROGRESS);
                i++;
                assertEquals(Status.IN_PROGRESS, task.getStatus());
            }
        }
    }

    @Test(expected = IndexIncorectException.class)
    public void testExceptionChangeTaskStatusByIndex() throws AbstractException {
        taskService.changeTaskStatusByIndex(USER_ID_1, -8, Status.IN_PROGRESS);
    }

    @Test
    public void testCreate() throws AbstractException {
        @NotNull final String name = "TEST project";
        @NotNull final String description = "DEscription";
        @NotNull final Task task = taskService.create(USER_ID_1, name, description);
        @NotNull final Task actualTask = taskRepository.findOneById(task.getId());
        assertEquals(name, actualTask.getName());
        assertEquals(description, actualTask.getDescription());
        assertEquals(USER_ID_1, actualTask.getUserId());
        assertEquals(taskRepository.getSize(), taskList.size() + 1);
    }

    @Test(expected = NameEmptyException.class)
    public void testExceptionCreate() throws AbstractException {
        @NotNull final String name = "";
        @NotNull final String description = "DEscription";
        taskService.create(USER_ID_1, name, description);
    }

    @Test
    public void testFindAllByProjectId() throws AbstractException {
        @NotNull final List<Task> tasksUser1 = new ArrayList<>();
        @NotNull final List<Task> tasksUser2 = new ArrayList<>();
        for (@NotNull final Task task : taskList) {
            if (USER_ID_1.equals(task.getUserId()) && projectUser1.getId().equals(task.getProjectId()))
                tasksUser1.add(task);
            if (USER_ID_2.equals(task.getUserId()) && projectUser1.getId().equals(task.getProjectId()))
                tasksUser2.add(task);
        }
        assertNotEquals(tasksUser1, tasksUser2);
        assertEquals(tasksUser1, taskService.findAllByProjectId(USER_ID_1, projectUser1.getId()));
        assertEquals(tasksUser2, taskService.findAllByProjectId(USER_ID_2, projectUser1.getId()));
    }

    @Test
    public void testUpdateById() throws AbstractException {
        for (@NotNull final Task task : taskList) {
            @NotNull final String name = task.getName() + "TEST";
            @NotNull final String description = task.getDescription() + "TEST";
            taskService.updateById(task.getUserId(), task.getId(), name, description);
            assertEquals(task.getName(), name);
            assertEquals(task.getDescription(), description);
        }
    }

    @Test
    public void testUpdateByIndex() throws AbstractException {
        int i = 0;
        for (@NotNull final Task task : taskList) {
            if (USER_ID_1.equals(task.getUserId())) {
                @NotNull final String name = task.getName() + "TEST";
                @NotNull final String description = task.getDescription() + "TEST";
                assertNotEquals(task.getName(), name);
                assertNotEquals(task.getDescription(), description);
                taskService.updateByIndex(task.getUserId(), i, name, description);
                i++;
                assertEquals(task.getName(), name);
                assertEquals(task.getDescription(), description);
            }
        }
    }
}
