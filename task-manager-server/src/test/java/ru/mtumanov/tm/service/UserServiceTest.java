package ru.mtumanov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import ru.mtumanov.tm.api.repository.IProjectRepository;
import ru.mtumanov.tm.api.repository.ITaskRepository;
import ru.mtumanov.tm.api.repository.IUserRepository;
import ru.mtumanov.tm.api.service.IPropertyService;
import ru.mtumanov.tm.api.service.IUserService;
import ru.mtumanov.tm.enumerated.Role;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.exception.field.EmailEmptyException;
import ru.mtumanov.tm.exception.field.IdEmptyException;
import ru.mtumanov.tm.exception.field.LoginEmptyException;
import ru.mtumanov.tm.exception.field.PasswordEmptyException;
import ru.mtumanov.tm.model.Project;
import ru.mtumanov.tm.model.Task;
import ru.mtumanov.tm.model.User;
import ru.mtumanov.tm.repository.ProjectRepository;
import ru.mtumanov.tm.repository.TaskRepository;
import ru.mtumanov.tm.repository.UserRepository;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class UserServiceTest {

    private static final int NUMBER_OF_ENTRIES = 10;

    @NotNull
    private static final IUserRepository userRepository = new UserRepository();

    @NotNull
    private static final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private static final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private static final IPropertyService propertyService = new PropertyService();

    @NotNull
    private static final IUserService userService = new UserService(userRepository, projectRepository, taskRepository, propertyService);

    @NotNull
    private static final List<User> userList = new ArrayList<>();

    @Before
    public void initRepository() {
        for (int i = 0; i < NUMBER_OF_ENTRIES; i++) {
            @NotNull final User user = new User();
            user.setFirstName("UserFirstName " + i);
            user.setLastName("UserLastName " + i);
            user.setMiddleName("UserMidName " + i);
            user.setEmail("user" + i + "@dot.ru");
            user.setLogin("USER" + i);
            user.setRole(Role.USUAL);
            userRepository.add(user);
            userList.add(user);

            for (int j = 0; j < NUMBER_OF_ENTRIES; j++) {
                @NotNull final Project project = new Project();
                project.setName("Project name: " + j);
                project.setDescription("Project description: " + j);
                project.setUserId(user.getId());
                projectRepository.add(project);

                for (int a = 0; a < NUMBER_OF_ENTRIES; a++) {
                    @NotNull final Task task = new Task();
                    task.setName("Task name: " + a);
                    task.setDescription("Task description: " + a);
                    task.setUserId(user.getId());
                    task.setProjectId(project.getId());
                    taskRepository.add(task);
                }
            }
        }
    }

    @After
    public void clearRepository() {
        userService.clear();
        projectRepository.clear();
        taskRepository.clear();
        userList.clear();
    }

    @Test
    public void testCreate() throws AbstractException {
        @NotNull List<User> userListActual = userRepository.findAll();
        assertEquals(userList, userListActual);

        userService.create("TEST_USER1", "TEST_PASSWORD");
        userListActual = userRepository.findAll();
        assertEquals(userList.size() + 1, userListActual.size());

        userService.create("TEST_USER2", "TEST_PASSWORD", "test@email.ru");
        userListActual = userRepository.findAll();
        assertEquals(userList.size() + 2, userListActual.size());

        userService.create("TEST_USER3", "TEST_PASSWORD", Role.USUAL);
        userListActual = userRepository.findAll();
        assertEquals(userList.size() + 3, userListActual.size());
    }

    @Test(expected = LoginEmptyException.class)
    public void testEmptyLoginCreate() throws AbstractException {
        userService.create("", "TEST_PASSWORD");
    }

    @Test(expected = PasswordEmptyException.class)
    public void testEmptyPswrdCreate() throws AbstractException {
        userService.create("LOGIN", "");
    }

    @Test(expected = LoginEmptyException.class)
    public void testEmptyLoginCreateWithEmail() throws AbstractException {
        userService.create("", "TEST_PASSWORD", "test@email.ru");
    }

    @Test(expected = PasswordEmptyException.class)
    public void testEmptyPswrdCreateWithEmail() throws AbstractException {
        userService.create("LOGIN", "", "test@email.ru");
    }

    @Test(expected = LoginEmptyException.class)
    public void testEmptyLoginCreateWithRole() throws AbstractException {
        userService.create("", "TEST_PASSWORD", Role.USUAL);
    }

    @Test(expected = PasswordEmptyException.class)
    public void testEmptyPswrdCreateWithRole() throws AbstractException {
        userService.create("LOGIN", "", Role.USUAL);
    }

    @Test
    public void testFindByEmail() throws AbstractException {
        for (@NotNull final User user : userList) {
            @NotNull final User actualUser = userService.findByEmail(user.getEmail());
            assertEquals(user, actualUser);
        }
    }

    @Test(expected = EmailEmptyException.class)
    public void testEmptyEmailFindByEmail() throws AbstractException {
        userService.findByEmail("");
    }

    @Test
    public void testFindByLogin() throws AbstractException {
        for (@NotNull final User user : userList) {
            @NotNull final User actualUser = userService.findByLogin(user.getLogin());
            assertEquals(user, actualUser);
        }
    }

    @Test(expected = LoginEmptyException.class)
    public void testEmptyLoginFindByLogin() throws AbstractException {
        userService.findByLogin("");
    }

    @Test
    public void testIsEmailExist() {
        for (@NotNull final User user : userList) {
            assertTrue(userService.isEmailExist(user.getEmail()));
            assertFalse(userService.isEmailExist(user.getEmail() + user.getEmail()));
            assertFalse(userService.isEmailExist(""));
        }
    }

    @Test
    public void testIsLoginExist() {
        for (@NotNull final User user : userList) {
            assertTrue(userService.isLoginExist(user.getLogin()));
            assertFalse(userService.isLoginExist(user.getLogin() + user.getLogin()));
            assertFalse(userService.isLoginExist(""));
        }
    }

    @Test
    public void testLockUserByLogin() throws AbstractException {
        for (@NotNull final User user : userList) {
            user.setLocked(false);
            assertFalse(user.isLocked());
            userService.lockUserByLogin(user.getLogin());
            assertTrue(user.isLocked());
        }
    }

    @Test(expected = LoginEmptyException.class)
    public void testLoginEmptyLockUserByLogin() throws AbstractException {
        userService.lockUserByLogin("");
    }

    @Test
    public void testRemoveByEmail() throws AbstractException {
        for (@NotNull final User user : userList) {
            assertTrue(userRepository.existById(user.getId()));
            assertNotEquals(0, projectRepository.findAll(user.getId()).size());
            assertNotEquals(0, taskRepository.findAll(user.getId()).size());
            userService.removeByEmail(user.getEmail());
            assertFalse(userRepository.existById(user.getId()));
            assertEquals(0, projectRepository.findAll(user.getId()).size());
            assertEquals(0, taskRepository.findAll(user.getId()).size());
        }
    }

    @Test
    public void testRemoveByLogin() throws AbstractException {
        for (@NotNull final User user : userList) {
            assertTrue(userRepository.existById(user.getId()));
            assertNotEquals(0, projectRepository.findAll(user.getId()).size());
            assertNotEquals(0, taskRepository.findAll(user.getId()).size());
            userService.removeByLogin(user.getLogin());
            assertFalse(userRepository.existById(user.getId()));
            assertEquals(0, projectRepository.findAll(user.getId()).size());
            assertEquals(0, taskRepository.findAll(user.getId()).size());
        }
    }

    @Test
    public void testSetPassword() throws AbstractException {
        for (@NotNull final User user : userList) {
            @NotNull final String oldHash = user.getPasswordHash();
            userService.setPassword(user.getId(), "STRONPASS12@qq" + user.getLogin());
            assertNotEquals(oldHash, user.getPasswordHash());
        }
    }

    @Test
    public void testUnlockUserByLogin() throws AbstractException {
        for (@NotNull final User user : userList) {
            user.setLocked(true);
            assertTrue(user.isLocked());
            userService.unlockUserByLogin(user.getLogin());
            assertFalse(user.isLocked());
        }
    }

    @Test(expected = LoginEmptyException.class)
    public void testLoginEmptyUnlockUserByLogin() throws AbstractException {
        userService.unlockUserByLogin("");
    }

    @Test
    public void testUserUpdate() throws AbstractException {
        for (@NotNull final User user : userList) {
            @NotNull final String firstName = user.getFirstName() + "TEST";
            @NotNull final String lastName = user.getLastName() + "TEST";
            @NotNull final String middleName = user.getMiddleName() + "TEST";
            assertNotEquals(user.getFirstName(), firstName);
            assertNotEquals(user.getLastName(), lastName);
            assertNotEquals(user.getMiddleName(), middleName);
            userService.userUpdate(user.getId(), firstName, lastName, middleName);
            assertEquals(user.getFirstName(), firstName);
            assertEquals(user.getLastName(), lastName);
            assertEquals(user.getMiddleName(), middleName);
        }
    }

    @Test(expected = IdEmptyException.class)
    public void testEmptyIdUserUpdate() throws AbstractException {
        userService.userUpdate("", "firstName", "lastName", "middleName");
    }
}
