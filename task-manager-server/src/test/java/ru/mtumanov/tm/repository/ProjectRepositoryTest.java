package ru.mtumanov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.junit.Before;
import org.junit.Test;
import ru.mtumanov.tm.api.repository.IProjectRepository;
import ru.mtumanov.tm.exception.entity.AbstractEntityNotFoundException;
import ru.mtumanov.tm.model.Project;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.junit.Assert.*;

;

public class ProjectRepositoryTest {

    private static final int NUMBER_OF_ENTRIES = 10;

    @NotNull
    private static final String USER_ID_1 = UUID.randomUUID().toString();

    @NotNull
    private static final String USER_ID_2 = UUID.randomUUID().toString();

    @NotNull
    private final List<Project> projectList = new ArrayList<>();

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @Before
    public void initRepository() {
        for (int i = 0; i < NUMBER_OF_ENTRIES; i++) {
            @NotNull final Project project = new Project();
            project.setName("Project name: " + i);
            project.setDescription("Project description: " + i);
            if (i <= 5)
                project.setUserId(USER_ID_1);
            else
                project.setUserId(USER_ID_2);
            projectList.add(project);
            projectRepository.add(project);
        }
    }

    @Test
    public void testAdd() {
        @NotNull final Project project = new Project();
        project.setName("Test project name");
        project.setDescription("Test project description");
        project.setUserId(USER_ID_1);

        projectList.add(project);
        projectRepository.add(USER_ID_1, project);
        @NotNull final List<Project> actualProjectList = projectRepository.findAll();
        assertEquals(projectList, actualProjectList);
    }

    @Test
    public void testClear() {
        assertNotEquals(0, projectRepository.getSize());
        projectRepository.clear();
        assertEquals(0, projectRepository.getSize());
    }

    @Test
    public void testExistById() {
        for (@NotNull final Project project : projectList) {
            assertTrue(projectRepository.existById(project.getUserId(), project.getId()));
        }
        assertFalse(projectRepository.existById(USER_ID_1, UUID.randomUUID().toString()));
    }

    @Test
    public void testFindAll() {
        @NotNull final List<Project> projectsUser1 = new ArrayList<>();
        @NotNull final List<Project> projectsUser2 = new ArrayList<>();
        for (@NotNull final Project project : projectList) {
            if (project.getUserId().equals(USER_ID_1))
                projectsUser1.add(project);
            if (project.getUserId().equals(USER_ID_2))
                projectsUser2.add(project);
        }
        assertNotEquals(projectsUser1, projectsUser2);

        @NotNull final List<Project> actualProjectsUser1 = projectRepository.findAll(USER_ID_1);
        @NotNull final List<Project> actualProjectsUser2 = projectRepository.findAll(USER_ID_2);
        assertEquals(projectsUser1, actualProjectsUser1);
        assertEquals(projectsUser2, actualProjectsUser2);
    }

    @Test
    public void testFindOneById() throws AbstractEntityNotFoundException {
        for (@NotNull final Project project : projectList) {
            assertEquals(project, projectRepository.findOneById(project.getUserId(), project.getId()));
        }
    }

    @Test(expected = AbstractEntityNotFoundException.class)
    public void testExceptionFindOneById() throws AbstractEntityNotFoundException {
        for (int i = 0; i < 10; i++) {
            projectRepository.findOneById(USER_ID_1, UUID.randomUUID().toString());
        }
    }

    @Test
    public void testFindOneByIndex() {
        int i = 0;
        int j = 0;
        for (@NotNull final Project project : projectList) {
            if (project.getUserId().equals(USER_ID_1)) {
                assertEquals(project, projectRepository.findOneByIndex(USER_ID_1, i));
                i++;
            }
            if (project.getUserId().equals(USER_ID_2)) {
                assertEquals(project, projectRepository.findOneByIndex(USER_ID_2, j));
                j++;
            }
        }
    }

    @Test
    public void testGetSize() {
        assertEquals(projectList.size(), projectRepository.getSize());
    }

    @Test
    public void testRemove() throws AbstractEntityNotFoundException {
        for (@NotNull final Project project : projectList) {
            assertTrue(projectRepository.existById(project.getId()));
            projectRepository.remove(project.getUserId(), project);
            assertFalse(projectRepository.existById(project.getId()));
        }
    }

    @Test(expected = AbstractEntityNotFoundException.class)
    public void testExceptionRemove() throws AbstractEntityNotFoundException {
        for (int i = 0; i < 10; i++) {
            projectRepository.remove(USER_ID_1, new Project());
        }
    }

    @Test
    public void testRemoveById() throws AbstractEntityNotFoundException {
        for (@NotNull final Project project : projectList) {
            assertTrue(projectRepository.existById(project.getId()));
            projectRepository.removeById(project.getUserId(), project.getId());
            assertFalse(projectRepository.existById(project.getId()));
        }
    }

    @Test(expected = AbstractEntityNotFoundException.class)
    public void testExceptionRemoveById() throws AbstractEntityNotFoundException {
        for (int i = 0; i < 10; i++) {
            projectRepository.removeById(USER_ID_2, UUID.randomUUID().toString());
        }
    }

    @Test
    public void testRemoveByIndex() {
        for (@NotNull final Project project : projectList) {
            if (project.getUserId().equals(USER_ID_1)) {
                assertTrue(projectRepository.existById(project.getId()));
                projectRepository.removeByIndex(project.getUserId(), 0);
                assertFalse(projectRepository.existById(project.getId()));
            }
            if (project.getUserId().equals(USER_ID_2)) {
                assertTrue(projectRepository.existById(project.getId()));
                projectRepository.removeByIndex(project.getUserId(), 0);
                assertFalse(projectRepository.existById(project.getId()));
            }
        }
    }
}
